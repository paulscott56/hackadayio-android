package paulscott.co.za.hackadayio_android.model;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * Created by paul on 2015/05/04.
 */
public class Repositories extends HackadayIOResponse {


    @Key(KEY_DATA)
    private List<Repository> repositories;

    public final List<Repository> getRepositories() {
        return repositories;
    }

    @Override
    public Repositories clone() {
        return (Repositories) super.clone();
    }

    @Override
    public Repositories set(String fieldName, Object value) {
        return (Repositories) super.set(fieldName, value);
    }

}
