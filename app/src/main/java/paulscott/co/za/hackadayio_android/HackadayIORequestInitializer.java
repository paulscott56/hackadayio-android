package paulscott.co.za.hackadayio_android;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest;
import com.google.api.client.googleapis.services.json.CommonGoogleJsonClientRequestInitializer;
import com.google.api.client.util.Preconditions;

import java.io.IOException;

public class HackadayIORequestInitializer extends CommonGoogleJsonClientRequestInitializer {

    private final Credential credential;

    public HackadayIORequestInitializer(Credential credential) {
        super();
        this.credential = Preconditions.checkNotNull(credential);
    }

    @Override
    protected void initializeJsonRequest(AbstractGoogleJsonClientRequest<?> request)
            throws IOException {
        super.initializeJsonRequest(request);
        initializeHackadayIORequest((HackadayIORequest<?>) request);
    }

    protected void initializeHackadayIORequest(HackadayIORequest<?> request)
            throws java.io.IOException {
        request.setAccessToken(credential.getAccessToken());
        
    }

}