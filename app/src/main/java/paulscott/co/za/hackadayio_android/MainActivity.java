package paulscott.co.za.hackadayio_android;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.api.client.auth.oauth2.ClientParametersAuthentication;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.HttpStatusCodes;
import com.google.api.client.util.Lists;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import paulscott.co.za.hackadayio_android.model.Feed;
import paulscott.co.za.hackadayio_android.model.FeedItem;
import paulscott.co.za.hackadayio_android.model.Meta;
import paulscott.co.za.hackadayio_android.ui.CompatArrayAdapter;
import paulscott.co.za.hackadayio_android.ui.ContentDecoratorAdapter;
import paulscott.co.za.hackadayio_android.ui.ListScrollListener;
import paulscott.co.za.hackadayio_android.ui.Loadable;
import paulscott.co.za.hackadayio_android.ui.LoadableDecorator;


public class MainActivity extends FragmentActivity {

    public static final String KEY_AUTH_MODE = "auth_mode";


    private SharedPreferences mPreference;
    private RequestQueue queue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fm = getSupportFragmentManager();

        queue = Volley.newRequestQueue(this);

        if (fm.findFragmentById(android.R.id.content) == null) {
            HackadayIOFeedListFragment list = new HackadayIOFeedListFragment();
            fm.beginTransaction().add(android.R.id.content, list).commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        Crouton.cancelAllCroutons();
        super.onDestroy();
    }


    public static class FeedAdapter extends CompatArrayAdapter<FeedItem> {

            private final LayoutInflater mInflater;

            public FeedAdapter(Context context) {
                super(context, R.layout.simple_list_item_image);
                mInflater = LayoutInflater.from(context);
            }

            public void setData(Feed feed, boolean clear) {
                if (clear) {
                    clear();
                }
                if (feed != null) {
                    List<FeedItem> feedItems = feed.getData();
                    if (feedItems != null) {
                        compatAddAll(feedItems);
                    }
                }
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view;
                if (convertView == null) {
                    view = mInflater
                            .inflate(R.layout.simple_list_item_image, parent, false);
                    ViewHolder holder = new ViewHolder(
                            (ImageView) view.findViewById(android.R.id.icon1),
                            (ImageView) view.findViewById(android.R.id.icon2),
                            (TextView) view.findViewById(android.R.id.text1));
                    view.setTag(holder);
                } else {
                    view = convertView;
                }

                ViewHolder holder = (ViewHolder) view.getTag();

                FeedItem item = getItem(position);
                String imageUrl = item.getImages().getThumbnail().getUrl();
                Picasso.with(getContext()).load(imageUrl).into(holder.imageView);
                String avatarUrl = item.getUser().getProfilePicture();
                Picasso.with(getContext()).load(avatarUrl).into(holder.avatarView);
                holder.usernameView.setText(item.getUser().getFullName());

                return view;
            }

            private static final class ViewHolder {

                ViewHolder(ImageView imageView, ImageView avatarView,
                           TextView usernameView) {
                    super();
                    this.imageView = imageView;
                    this.avatarView = avatarView;
                    this.usernameView = usernameView;
                }

                ImageView imageView;
                ImageView avatarView;
                TextView usernameView;
            }

    }

    public static class HackadayIOLoader extends AsyncResourceLoader<Feed> {

        //private final String nextMaxId;
        private final OAuth oauth;
        private final int page;

        public HackadayIOLoader(FragmentActivity activity, int since) {
            super(activity);
            //this.nextMaxId = nextMaxId;
            this.page = since;
            this.oauth = OAuth.newInstance(activity.getApplicationContext(),
                    activity.getFragmentManager(),
                    new ClientParametersAuthentication(HackadayIOConstants.CLIENT_ID,
                            HackadayIOConstants.CLIENT_SECRET),
                    HackadayIOConstants.AUTHORIZATION_CODE_SERVER_URL,
                    HackadayIOConstants.TOKEN_SERVER_URL,
                    HackadayIOConstants.REDIRECT_URL,
                    Lists.<String> newArrayList());
        }

        public boolean isLoadMoreRequest() {
            return page != 0;
        }

        @Override
        public Feed loadResourceInBackground() throws Exception {
            Credential credential = oauth.authorizeExplicitly(
                    "HackadayIO-Android").getResult();

            HackadayIO hackadayio =
                    new HackadayIO.Builder(OAuth.HTTP_TRANSPORT, OAuth.JSON_FACTORY, null)
                            .setApplicationName(getContext().getString(R.string.app_name_long))
                            .setHackadayIORequestInitializer(
                                    new HackadayIORequestInitializer(credential))
                            .build();
            HackadayIO.Users.Self.FeedRequest request = hackadayio.users().self().feed();
            if (isLoadMoreRequest()) {
                request.setPage(page);
            }
            //request.set("api_key", HackadayIOConstants.API_KEY);
            Feed feed = request.execute();
            LOGGER.info(request.getJsonContent().toString());
            LOGGER.info(feed.toString());
            return feed;
        }

        @Override
        public void updateErrorStateIfApplicable(AsyncResourceLoader.Result<Feed> result) {
            Feed data = result.data;
            Meta meta = data.getMeta();
            result.success = meta.getCode() == HttpStatusCodes.STATUS_CODE_OK;
            result.errorMessage = result.success ? null :
                    (meta.getCode() + " " + meta.getErrorType() + ": " + meta.getErrorMessage());
        }

    }


    public static class HackadayIOFeedListFragment extends ListFragment implements
            LoaderManager.LoaderCallbacks<AsyncResourceLoader.Result<Feed>> {

        FeedAdapter mAdapter;
        Loadable<Feed> mLoadable;

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            setHasOptionsMenu(true);

            mAdapter = new FeedAdapter(getActivity().getApplicationContext());
            mLoadable = new FeedLoadable(getLoaderManager(), 0,
                    new LoadableDecorator<Feed>(this, 0, this));
            setListAdapter(new ContentDecoratorAdapter(mLoadable, mAdapter));
            getListView().setOnScrollListener(new ListScrollListener(mLoadable));

            mLoadable.init();
        }

        @Override
        public Loader<AsyncResourceLoader.Result<Feed>> onCreateLoader(int id, Bundle args) {
            return new HackadayIOLoader(getActivity(), FeedLoadable.getPage(args));
        }

        @Override
        public void onLoadFinished(Loader<AsyncResourceLoader.Result<Feed>> loader, AsyncResourceLoader.Result<Feed> result) {
            final boolean clear = !((HackadayIOLoader) loader).isLoadMoreRequest();
            mAdapter.setData(result.data, clear);
        }

        @Override
        public void onLoaderReset(Loader<AsyncResourceLoader.Result<Feed>> loader) {
            mAdapter.setData(null, true);
        }

        @Override
        public void onDestroy() {
            mLoadable.destroy();
            super.onDestroy();
        }

    }
}