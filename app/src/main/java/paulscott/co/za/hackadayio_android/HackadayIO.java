package paulscott.co.za.hackadayio_android;

/**
 * Created by paul on 2015/05/04.
 */

import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient;
import com.google.api.client.http.HttpMethods;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.util.Key;

import java.io.IOException;

import paulscott.co.za.hackadayio_android.model.Feed;

public class HackadayIO extends AbstractGoogleJsonClient {

    public static final String DEFAULT_ROOT_URL = HackadayIOConstants.GLOBAL_FEEDS_URI;

    public static final String DEFAULT_SERVICE_PATH = "v1/feeds/global";

    public static final String DEFAULT_BASE_URL = DEFAULT_ROOT_URL + DEFAULT_SERVICE_PATH;

    public HackadayIO(com.google.api.client.http.HttpTransport transport,
                      com.google.api.client.json.JsonFactory jsonFactory,
                      com.google.api.client.http.HttpRequestInitializer httpRequestInitializer) {
        this(new Builder(transport, jsonFactory, httpRequestInitializer));
    }

    HackadayIO(Builder builder) {
        super(builder);
    }

    @Override
    protected void initialize(AbstractGoogleClientRequest<?> httpClientRequest) throws IOException {
        super.initialize(httpClientRequest);
    }

    public Users user() {
        return new Users();
    }

    public static final class Builder extends
            com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient.Builder {

        public Builder(com.google.api.client.http.HttpTransport transport,
                       com.google.api.client.json.JsonFactory jsonFactory,
                       com.google.api.client.http.HttpRequestInitializer httpRequestInitializer) {
            super(transport,
                    jsonFactory,
                    DEFAULT_ROOT_URL,
                    DEFAULT_SERVICE_PATH,
                    httpRequestInitializer,
                    false);
        }

        @Override
        public HackadayIO build() {
            return new HackadayIO(this);
        }

        @Override
        public Builder setRootUrl(String rootUrl) {
            return (Builder) super.setRootUrl(rootUrl);
        }

        @Override
        public Builder setServicePath(String servicePath) {
            return (Builder) super.setServicePath(servicePath);
        }

        @Override
        public Builder setGoogleClientRequestInitializer(
                GoogleClientRequestInitializer googleClientRequestInitializer) {
            return (Builder) super
                    .setGoogleClientRequestInitializer(googleClientRequestInitializer);
        }

        @Override
        public Builder setHttpRequestInitializer(HttpRequestInitializer httpRequestInitializer) {
            return (Builder) super.setHttpRequestInitializer(httpRequestInitializer);
        }

        @Override
        public Builder setApplicationName(String applicationName) {
            return (Builder) super.setApplicationName(applicationName);
        }

        @Override
        public Builder setSuppressPatternChecks(boolean suppressPatternChecks) {
            return (Builder) super.setSuppressPatternChecks(suppressPatternChecks);
        }

        @Override
        public Builder setSuppressRequiredParameterChecks(boolean suppressRequiredParameterChecks) {
            return (Builder) super
                    .setSuppressRequiredParameterChecks(suppressRequiredParameterChecks);
        }

        @Override
        public Builder setSuppressAllChecks(boolean suppressAllChecks) {
            return (Builder) super.setSuppressAllChecks(suppressAllChecks);
        }


        public Builder setHackadayIORequestInitializer(
                HackadayIORequestInitializer hackadayIORequestInitializer) {
            return (Builder) super.setGoogleClientRequestInitializer(hackadayIORequestInitializer);
        }

    }

    public Users users() {
        return new Users();
    }

    public class Users {

        public Self self() {
            return new Self();
        }

        public class Self {

            public FeedRequest feed() throws IOException {
                FeedRequest result = new FeedRequest();
                initialize(result);
                return result;
            }

            public class FeedRequest
                    extends
                    HackadayIORequest<Feed> {

                private static final String REST_PATH = "v1/feeds/global";
                @Key("total")
                private Integer count;

                @Key("min_id")
                private String minId;

                @Key("max_id")
                private String maxId;

                @Key("page")
                private Integer page;

                protected FeedRequest() {
                    super(HackadayIO.this,
                            HttpMethods.GET,
                            REST_PATH,
                            null,
                            Feed.class);
                }

                public final FeedRequest setCount(Integer count) {
                    this.count = count;
                    return this;
                }

                public final FeedRequest setMinId(String minId) {
                    this.minId = minId;
                    return this;
                }

                public final FeedRequest setMaxId(String maxId) {
                    this.maxId = maxId;
                    return this;
                }

                @Override
                public FeedRequest setAccessToken(String accessToken) {
                    return (FeedRequest) super.setAccessToken(accessToken);
                }

                @Override
                public FeedRequest set(String fieldName, Object value) {
                    return (FeedRequest) super.set(fieldName, value);
                }

                @Override
                public HttpRequest buildHttpRequestUsingHead() throws IOException {
                    return super.buildHttpRequestUsingHead();
                }

                @Override
                public HttpResponse executeUsingHead() throws IOException {
                    return super.executeUsingHead();
                }


                public final FeedRequest setPage(Integer page) {
                    this.page = page;
                    return this;
                }
                public final Integer getPage() {
                    return page;
                }

            }

        }

    }
}