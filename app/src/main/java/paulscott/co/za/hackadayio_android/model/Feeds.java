package paulscott.co.za.hackadayio_android.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

/**
 * Created by paul on 2015/05/05.
 */
public class Feeds extends GenericJson {
    /**
     * Global feeds model
     */

    @Key("id")
    private Integer id;

    @Key("user_id")
    private Integer userId;

    @Key("project_id")
    private Integer projectId;

    @Key("user2_id")
    private Integer user2Id;

    @Key("post_id")
    private Integer postId;

    @Key("type")
    private String type;

    @Key("activity")
    private String activity;

    @Key("json")
    private FeedJSON feedjson;

    @Key("created")
    private Integer created;


    public final Integer getId() {
        return id;
    }

    public final Integer getUserId() {
        return userId;
    }

    public final Integer getProjectId() {
        return projectId;
    }

    public final Integer getUser2Id() {
        return user2Id;
    }

    public final Integer getPostId() {
        return postId;
    }

    public final String getType() {
        return type;
    }

    public final String getActivity() {
        return activity;
    }

    public final FeedJSON getFeedjson() {
        return feedjson;
    }

    public final Integer getCreated() {
        return created;
    }

    @Override
    public Feeds clone() {
        return (Feeds) super.clone();
    }

    @Override
    public Feeds set(String fieldName, Object value) {
        return (Feeds) super.set(fieldName, value);
    }
}