package paulscott.co.za.hackadayio_android;

/**
 * Created by paul on 2015/05/04.
 */
public class HackadayIOConstants {

    public static final String CLIENT_ID = "3aih05g8fGJmCQpnVdXIb8rpN5iGFH9LdhJmK6G0dfeqQS0D";

    public static final String CLIENT_SECRET = "Z13hZo6tHeJFsBF9uWF9bwCukdLRIT5DqCwoJv580XymflLT";

    public static final String AUTHORIZATION_CODE_SERVER_URL = "https://hackaday.io/authorize?client_id=3aih05g8fGJmCQpnVdXIb8rpN5iGFH9LdhJmK6G0dfeqQS0D&response_type=code";

    public static final String API_KEY = "fkXPcIluWCvA1YKY";

    public static final String TOKEN_SERVER_URL = "https://auth.hackaday.io/access_token";

    public static final String REDIRECT_URL = "http://localhost/Callback";

    public static final String TAG = "HackadayIO";

    public static final String CREDENTIALS_STORE_PREF_FILE = "oauth";

    public static final String GLOBAL_FEEDS_URI = "https://api.hackaday.io/";


    private HackadayIOConstants() {

    }
}
