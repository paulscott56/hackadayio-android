package paulscott.co.za.hackadayio_android.model;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * Created by paul on 2015/05/05.
 */
public class GlobalFeeds extends HackadayIOResponse {


    @Key(KEY_DATA)
    private List<Feeds> feeds;

    public final List<Feeds> getFeeds() {
        return feeds;
    }

    @Override
    public GlobalFeeds clone() {
        return (GlobalFeeds) super.clone();
    }

    @Override
    public GlobalFeeds set(String fieldName, Object value) {
        return (GlobalFeeds) super.set(fieldName, value);
    }
}
