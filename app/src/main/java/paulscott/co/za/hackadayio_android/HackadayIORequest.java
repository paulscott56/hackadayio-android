package paulscott.co.za.hackadayio_android;

/**
 * Created by paul on 2015/05/04.
 */

import com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.util.Key;

public class HackadayIORequest<T> extends AbstractGoogleJsonClientRequest<T> {

    public HackadayIORequest(HackadayIO client, String requestMethod,
                            String uriTemplate, Object content, Class<T> responseClass) {
        super(client,
                requestMethod,
                uriTemplate,
                content,
                responseClass);
    }

    @Key("access_token")
    private String accessToken;

    @Key("api_key")
    private String apiKey = HackadayIOConstants.API_KEY;

    public final String getAccessToken() {
        return accessToken;
    }

    public HackadayIORequest<T> setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    @Override
    public HackadayIO getAbstractGoogleClient() {
        return (HackadayIO) super.getAbstractGoogleClient();
    }

    @Override
    public HackadayIORequest<T> setDisableGZipContent(boolean disableGZipContent) {
        return (HackadayIORequest<T>) super.setDisableGZipContent(disableGZipContent);
    }

    @Override
    public HackadayIORequest<T> setRequestHeaders(HttpHeaders headers) {
        return (HackadayIORequest<T>) super.setRequestHeaders(headers);
    }

    @Override
    public HackadayIORequest<T> set(String fieldName, Object value) {
        return (HackadayIORequest<T>) super.set(fieldName, value);
    }

    @Override
    public HackadayIORequest<T> getJsonContent() {
        return (HackadayIORequest<T>) super.getJsonContent();
    }


}