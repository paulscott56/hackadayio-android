package paulscott.co.za.hackadayio_android.ui;

/**
 * Created by paul on 2015/05/04.
 */

import android.support.v4.app.LoaderManager.LoaderCallbacks;

import paulscott.co.za.hackadayio_android.AsyncResourceLoader;

public interface Loadable<T> extends LoaderCallbacks<AsyncResourceLoader.Result<T>> {

    boolean hasMore();

    boolean hasError();

    void init();

    void destroy();

    boolean isReadyToLoadMore();

    void loadMore();
}
