package paulscott.co.za.hackadayio_android.model;

/**
 * Created by paul on 2015/05/04.
 */
public abstract class HackadayIOResponse extends ClientErrors {

    public static final String KEY_DATA = "data";

    private Pagination pagination;

    public final Pagination getPagination() {
        return pagination;
    }

    public final HackadayIOResponse setPagination(Pagination pagination) {
        this.pagination = pagination;
        return this;
    }

    @Override
    public HackadayIOResponse clone() {
        return (HackadayIOResponse) super.clone();
    }

    @Override
    public HackadayIOResponse set(String fieldName, Object value) {
        return (HackadayIOResponse) super.set(fieldName, value);
    }
}